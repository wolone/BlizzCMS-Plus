<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = '仪表盘';
$lang['admin_nav_system'] = '系统';
$lang['admin_nav_manage_settings'] = '设置管理';
$lang['admin_nav_manage_modules'] = '模块管理';
$lang['admin_nav_users'] = '用户';
$lang['admin_nav_accounts'] = '账户';
$lang['admin_nav_website'] = '网站';
$lang['admin_nav_menu'] = '菜单';
$lang['admin_nav_realms'] = '服务器';
$lang['admin_nav_slides'] = '幻灯片';
$lang['admin_nav_news'] = '新闻';
$lang['admin_nav_changelogs'] = '更新日志';
$lang['admin_nav_pages'] = '页面';
$lang['admin_nav_donate_methods'] = '捐赠方式';
$lang['admin_nav_topsites'] = 'Topsites';
$lang['admin_nav_donate_vote_logs'] = '捐赠/投票日志';
$lang['admin_nav_store'] = '商店';
$lang['admin_nav_manage_store'] = '商店管理';
$lang['admin_nav_forum'] = '社区';
$lang['admin_nav_manage_forum'] = '社区管理';
$lang['admin_nav_logs'] = '系统日志';

/*Sections Lang*/
$lang['section_general_settings'] = '常规设置';
$lang['section_module_settings'] = '模块设置';
$lang['section_optional_settings'] = '可选设置';
$lang['section_seo_settings'] = 'SEO设置';
$lang['section_update_cms'] = '更新CMS';
$lang['section_check_information'] = '核对信息';
$lang['section_forum_categories'] = '社区分类';
$lang['section_forum_elements'] = '社区版块';
$lang['section_store_categories'] = '商店分类';
$lang['section_store_items'] = '商店物品';
$lang['section_store_top'] = '商店推荐物品';
$lang['section_logs_dp'] = '捐赠日志';
$lang['section_logs_vp'] = '投票日志';

/*Button Lang*/
$lang['button_select'] = '选择';
$lang['button_update'] = '更新';
$lang['button_unban'] = '解禁';
$lang['button_ban'] = '封禁';
$lang['button_remove'] = '删除';
$lang['button_grant'] = '允许';
$lang['button_update_version'] = '更新至最新版本';

/*Table header Lang*/
$lang['table_header_subcategory'] = '选择分类';
$lang['table_header_race'] = '种族';
$lang['table_header_class'] = '分类';
$lang['table_header_level'] = '等级';
$lang['table_header_money'] = '钱币';
$lang['table_header_time_played'] = '游戏时长';
$lang['table_header_actions'] = '操作';
$lang['table_header_id'] = '编号';
$lang['table_header_tax'] = '税收';
$lang['table_header_points'] = '点数';
$lang['table_header_type'] = '类型';
$lang['table_header_module'] = '模块';
$lang['table_header_payment_id'] = 'Payment ID';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Total';
$lang['table_header_create_time'] = '创建时间';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Information';
$lang['table_header_value'] = '价值';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = '账户管理';
$lang['placeholder_update_information'] = '更新帐户信息';
$lang['placeholder_donation_logs'] = '捐赠日志';
$lang['placeholder_store_logs'] = '商店日志';
$lang['placeholder_create_changelog'] = '创建更新日志';
$lang['placeholder_edit_changelog'] = '编辑更新日志';
$lang['placeholder_create_category'] = '创建分类';
$lang['placeholder_edit_category'] = '编辑分类';
$lang['placeholder_create_forum'] = '创建社区';
$lang['placeholder_edit_forum'] = '编辑社区';
$lang['placeholder_create_menu'] = '创建菜单';
$lang['placeholder_edit_menu'] = '编辑菜单';
$lang['placeholder_create_news'] = '创建新闻';
$lang['placeholder_edit_news'] = '编辑新闻';
$lang['placeholder_create_page'] = '创建页面';
$lang['placeholder_edit_page'] = '编辑页面';
$lang['placeholder_create_realm'] = '添加服务器';
$lang['placeholder_edit_realm'] = '编辑服务器';
$lang['placeholder_create_slide'] = '创建幻灯片';
$lang['placeholder_edit_slide'] = '编辑幻灯片';
$lang['placeholder_create_item'] = '创建物品';
$lang['placeholder_edit_item'] = '编辑物品';
$lang['placeholder_create_topsite'] = '创建置顶';
$lang['placeholder_edit_topsite'] = '编辑置顶';
$lang['placeholder_create_top'] = '创建推荐物品';
$lang['placeholder_edit_top'] = '编辑推荐物品';

$lang['placeholder_upload_image'] = '上传图片';
$lang['placeholder_icon_name'] = '图标名称';
$lang['placeholder_category'] = '分类';
$lang['placeholder_name'] = '名称';
$lang['placeholder_item'] = '物品';
$lang['placeholder_image_name'] = '图片名称';
$lang['placeholder_reason'] = '原因';
$lang['placeholder_gmlevel'] = 'GM等级';
$lang['placeholder_url'] = '链接';
$lang['placeholder_child_menu'] = '子菜单';
$lang['placeholder_url_type'] = '链接类型';
$lang['placeholder_route'] = '线路';
$lang['placeholder_hours'] = '小时';
$lang['placeholder_soap_hostname'] = 'Soap主机名';
$lang['placeholder_soap_port'] = 'Soap端口';
$lang['placeholder_soap_user'] = 'Soap用户名';
$lang['placeholder_soap_password'] = 'Soap密码';
$lang['placeholder_db_character'] = '角色';
$lang['placeholder_db_hostname'] = '数据库主机名';
$lang['placeholder_db_name'] = '数据库名称';
$lang['placeholder_db_user'] = '数据库用户名';
$lang['placeholder_db_password'] = '数据库密码';
$lang['placeholder_account_points'] = '账户点数';
$lang['placeholder_account_ban'] = '封禁账户';
$lang['placeholder_account_unban'] = '解禁账户';
$lang['placeholder_account_grant_rank'] = '授权GM等级';
$lang['placeholder_account_remove_rank'] = '删除GM等级';
$lang['placeholder_command'] = '命令';
$lang['placeholder_emulator'] = '核心';

/*Config Lang*/
$lang['conf_website_name'] = '网站名称';
$lang['conf_realmlist'] = '服务器列表';
$lang['conf_discord_invid'] = 'Discord Invitation ID';
$lang['conf_timezone'] = '时区';
$lang['conf_theme_name'] = '主题名称';
$lang['conf_maintenance_mode'] = '维护模式';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'PayPal Currency';
$lang['conf_paypal_mode'] = 'PayPal Mode';
$lang['conf_paypal_client'] = 'PayPal Client ID';
$lang['conf_paypal_secretpass'] = 'PayPal Secret Password';
$lang['conf_default_description'] = 'Default Description';
$lang['conf_admin_gmlvl'] = '管理员级别';
$lang['conf_mod_gmlvl'] = '版主级别';
$lang['conf_recaptcha_key'] = 'reCaptcha Site Key';
$lang['conf_account_activation'] = '激活账户';
$lang['conf_smtp_hostname'] = 'SMTP主机名';
$lang['conf_smtp_port'] = 'SMTP端口';
$lang['conf_smtp_encryption'] = 'SMTP加密';
$lang['conf_smtp_username'] = 'SMTP用户名';
$lang['conf_smtp_password'] = 'SMTP密码';
$lang['conf_sender_email'] = '发件人电子邮件';
$lang['conf_sender_name'] = '发件人名称';

/*Logs */
$lang['placeholder_logs_dp'] = '捐赠';
$lang['placeholder_logs_quantity'] = '数量';
$lang['placeholder_logs_hash'] = 'Hash';
$lang['placeholder_logs_voteid'] = '投票ID';
$lang['placeholder_logs_points'] = '点数';
$lang['placeholder_logs_lasttime'] = '发布时间';
$lang['placeholder_logs_expiredtime'] = '失效时间';

/*Status Lang*/
$lang['status_completed'] = '完成';
$lang['status_cancelled'] = '取消';

/*Options Lang*/
$lang['option_normal'] = '正常';
$lang['option_dropdown'] = '下拉';
$lang['option_image'] = '图片';
$lang['option_video'] = '视频';
$lang['option_iframe'] = '框架';
$lang['option_enabled'] = '开启';
$lang['option_disabled'] = '关闭';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = '所有人';
$lang['option_staff'] = '管理人员';
$lang['option_all'] = '全体人员';
$lang['option_rename'] = '重命名';
$lang['option_customize'] = 'Customize';
$lang['option_change_faction'] = '变更阵营';
$lang['option_change_race'] = '变更种族';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP & VP';
$lang['option_internal_url'] = 'Internal URL';
$lang['option_external_url'] = 'External URL';
$lang['option_on'] = '开';
$lang['option_off'] = '关';

/*Count Lang*/
$lang['count_accounts_created'] = '注册账户';
$lang['count_accounts_banned'] = '封禁账号';
$lang['count_news_created'] = '发布新闻';
$lang['count_changelogs_created'] = '更新日志';
$lang['total_accounts_registered'] = '已注册账户总数';
$lang['total_accounts_banned'] = '已封禁账户总数';
$lang['total_news_writed'] = '已发布新闻';
$lang['total_changelogs_writed'] = '已更新日志';

$lang['info_alliance_players'] = '联盟';
$lang['info_alliance_playing'] = '联盟人数';
$lang['info_horde_players'] = '部落';
$lang['info_horde_playing'] = '部落人数';
$lang['info_players_playing'] = '在线人数';

/*Alert Lang*/
$lang['alert_smtp_activation'] = '如果启用此选项，则必须将SMTP配置为发送电子邮件.';
$lang['alert_banned_reason'] = '被禁止，原因:';

/*Logs Lang*/
$lang['log_new_level'] = '接收新级别';
$lang['log_old_level'] = '之前';
$lang['log_new_name'] = '它有一个新名字';
$lang['log_old_name'] = '之前';
$lang['log_unbanned'] = 'Unbanned';
$lang['log_customization'] = 'Get a customization';
$lang['log_change_race'] = 'Get a Race Change';
$lang['log_change_faction'] = 'Get a Faction Change';
$lang['log_banned'] = 'Was banned';
$lang['log_gm_assigned'] = 'Received GM rank';
$lang['log_gm_removed'] = 'The GM rank was removed';

/*CMS Lang*/
$lang['cms_version_currently'] = '此版本当前正在运行';
$lang['cms_warning_update'] = '更新后，根据对每个版本所做的更改，可以将配置恢复为默认值.';
$lang['cms_php_version'] = 'PHP版本';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = '加载的模块';
$lang['cms_loaded_extensions'] = '加载的扩展';

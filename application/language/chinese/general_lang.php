<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = '新闻';
$lang['tab_forum'] = '社区';
$lang['tab_store'] = '商店';
$lang['tab_bugtracker'] = 'BUG记录';
$lang['tab_changelogs'] = '更新日志';
$lang['tab_pvp_statistics'] = 'PvP统计';
$lang['tab_login'] = '登录';
$lang['tab_register'] = '注册';
$lang['tab_home'] = '首页';
$lang['tab_donate'] = '捐赠';
$lang['tab_vote'] = '投票';
$lang['tab_cart'] = '购物车';
$lang['tab_account'] = '我的账户';
$lang['tab_reset'] = '密码重置';
$lang['tab_error'] = 'Error 404';
$lang['tab_maintenance'] = '维护';
$lang['tab_online'] = '在线玩家';

/*Panel Navbar*/
$lang['navbar_vote_panel'] = '投票面板';
$lang['navbar_donate_panel'] = '捐赠面板';

/*Button Lang*/
$lang['button_register'] = '注册';
$lang['button_login'] = '登陆';
$lang['button_logout'] = '注销';
$lang['button_forgot_password'] = '忘记密码?';
$lang['button_user_panel'] = '用户面板';
$lang['button_admin_panel'] = '管理面板';
$lang['button_mod_panel'] = '模型面板';
$lang['button_change_avatar'] = '更改头像';
$lang['button_add_personal_info'] = '添加个人信息';
$lang['button_create_report'] = '提交BUG';
$lang['button_new_topic'] = '新话题';
$lang['button_edit_topic'] = '编辑主题';
$lang['button_save_changes'] = '保存更改';
$lang['button_cancel'] = '取消';
$lang['button_send'] = '发送';
$lang['button_read_more'] = '阅读更多';
$lang['button_add_reply'] = '回复';
$lang['button_remove'] = '删除';
$lang['button_create'] = '创建';
$lang['button_save'] = '保存';
$lang['button_close'] = '关闭';
$lang['button_reply'] = '回复';
$lang['button_donate'] = '捐赠';
$lang['button_account_settings'] = '账户设置';
$lang['button_cart'] = '添加购物车';
$lang['button_view_cart'] = '查看购物车';
$lang['button_checkout'] = '结算';
$lang['button_buying'] = '继续购买';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Item purchased successfully.';
$lang['alert_upload_error'] = 'Your image must be in jpg or png format';
$lang['alert_changelog_not_found'] = '暂无记录';
$lang['alert_points_insufficient'] = 'Insufficient points';

/*Status Lang*/
$lang['offline'] = '离线';
$lang['online'] = '在线';

/*Label Lang*/
$lang['label_open'] = '打开';
$lang['label_closed'] = '关闭';

/*Form Label Lang*/
$lang['label_login_info'] = 'Login Information';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = '用户名';
$lang['placeholder_email'] = '邮箱';
$lang['placeholder_password'] = '密码';
$lang['placeholder_re_password'] = '重复密码';
$lang['placeholder_current_password'] = '当前密码';
$lang['placeholder_new_password'] = '新密码';
$lang['placeholder_new_username'] = '新用户名';
$lang['placeholder_confirm_username'] = '确认新用户名';
$lang['placeholder_new_email'] = '新邮箱';
$lang['placeholder_confirm_email'] = '确认新邮箱';
$lang['placeholder_create_bug_report'] = '提交BUG';
$lang['placeholder_title'] = '标题';
$lang['placeholder_type'] = '类别';
$lang['placeholder_description'] = '描述';
$lang['placeholder_url'] = '链接';
$lang['placeholder_uri'] = 'Friendly URL (Example: tos)';
$lang['placeholder_highl'] = '高亮';
$lang['placeholder_lock'] = '锁定';
$lang['placeholder_subject'] = '主题';

/*Table header Lang*/
$lang['table_header_name'] = '名字';
$lang['table_header_faction'] = '阵营';
$lang['table_header_total_kills'] = '总死亡人数';
$lang['table_header_today_kills'] = '今天杀了我';
$lang['table_header_yersterday_kills'] = '昨天死了';
$lang['table_header_team_name'] = '团队名称';
$lang['table_header_members'] = '成员';
$lang['table_header_rating'] = '评级';
$lang['table_header_games'] = '游戏';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = '状态';
$lang['table_header_priority'] = '级别';
$lang['table_header_date'] = '日期';
$lang['table_header_author'] = '作者';
$lang['table_header_time'] = '时间';
$lang['table_header_icon'] = '图标';
$lang['table_header_realm'] = '服务器';
$lang['table_header_zone'] = '区域';
$lang['table_header_character'] = '角色';
$lang['table_header_price'] = '价格';
$lang['table_header_item_name'] = '物品名称';
$lang['table_header_items'] = '物品';
$lang['table_header_quantity'] = '数量';

/*Class Lang*/
$lang['class_warrior'] = '战士';
$lang['class_paladin'] = '圣骑士';
$lang['class_hunter'] = '猎人';
$lang['class_rogue'] = '潜行者';
$lang['class_priest'] = '牧师';
$lang['class_dk'] = '死亡骑士';
$lang['class_shamman'] = '萨满';
$lang['class_mage'] = '法师';
$lang['class_warlock'] = '术士';
$lang['class_monk'] = '武僧';
$lang['class_druid'] = '德鲁伊';
$lang['class_demonhunter'] = '恶魔猎手';

/*Faction Lang*/
$lang['faction_alliance'] = '联盟';
$lang['faction_horde'] = '部落';

/*Gender Lang*/
$lang['gender_male'] = '男';
$lang['gender_female'] = '女';

/*Race Lang*/
$lang['race_human'] = '人类';
$lang['race_orc'] = '兽人';
$lang['race_dwarf'] = '侏儒';
$lang['race_night_elf'] = '暗夜精灵';
$lang['race_undead'] = '亡灵';
$lang['race_tauren'] = '牛头人';
$lang['race_gnome'] = '地精';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Goblin';
$lang['race_blood_elf'] = 'Blood Elf';
$lang['race_draenei'] = '德莱尼';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Pandaren Neutral';
$lang['race_panda_alli'] = 'Pandaren Alliance';
$lang['race_panda_horde'] = 'Pandaren Horde';
$lang['race_nightborde'] = 'Nightborne';
$lang['race_void_elf'] = 'Void Elf';
$lang['race_lightforged_draenei'] = 'Lightforged Draenei';
$lang['race_highmountain_tauren'] = 'Highmountain Tauren';
$lang['race_dark_iron_dwarf'] = 'Dark Iron Dwarf';
$lang['race_maghar_orc'] = 'Maghar Orc';

/*Header Lang*/
$lang['header_cookie_message'] = '本网站使用cookies确保您在我们的网站上获得最佳体验';
$lang['header_cookie_button'] = '知道了!';

/*Footer Lang*/
$lang['footer_rights'] = 'All rights reserved.';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Page not found';
$lang['page_404_description'] = '似乎找不到您正在查找的页面';

/*Panel Lang*/
$lang['panel_acc_rank'] = '账户等级';
$lang['panel_dp'] = '捐赠点';
$lang['panel_vp'] = '投票点';
$lang['panel_expansion'] = '扩展';
$lang['panel_member'] = '成员';
$lang['panel_chars_list'] = '字符列表';
$lang['panel_account_details'] = '账户详细信息';
$lang['panel_last_ip'] = '最后IP';
$lang['panel_change_email'] = '更改邮箱';
$lang['panel_change_username'] = '更改用户名';
$lang['panel_change_password'] = '修改密码';
$lang['panel_replace_pass_by'] = '将密码更改为';
$lang['panel_current_username'] = '当前用户名';
$lang['panel_current_email'] = '当前邮箱';
$lang['panel_replace_email_by'] = '将邮箱更改为';

/*Home Lang*/
$lang['home_latest_news'] = '最新新闻';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = '服务器状态';
$lang['home_realm_info'] = '当前服务器';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = '前20名';
$lang['statistics_top_2v2'] = '2V2排名';
$lang['statistics_top_3v3'] = '3V3排名';
$lang['statistics_top_5v5'] = '5V5排名';

/*News Lang*/
$lang['news_recent_list'] = '新闻列表';
$lang['news_comments'] = '评论';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = '暂无记录';

/*Donate Lang*/
$lang['donate_get'] = '获取';

/*Vote Lang*/
$lang['vote_next_time'] = '下一次投票:';

/*Forum Lang*/
$lang['forum_posts_count'] = '帖子';
$lang['forum_topic_locked'] = '此主题已锁定';
$lang['forum_comment_locked'] = '有什么要说的吗？登录以加入对话。';
$lang['forum_comment_header'] = '加入对话';
$lang['forum_not_authorized'] = '未授权';
$lang['forum_post_history'] = '查看发布历史记录';
$lang['forum_topic_list'] = '主题列表';
$lang['forum_last_activity'] = '最新活动';
$lang['forum_last_post_by'] = '上次发布者';
$lang['forum_whos_online'] = '在线用户';
$lang['forum_replies_count'] = '回复';
$lang['forum_topics_count'] = '话题';
$lang['forum_users_count'] = '用户';

/*Store Lang*/
$lang['store_categories'] = '商店类别';
$lang['store_top_items'] = '物品排行';
$lang['store_cart_added'] = '您已添加';
$lang['store_cart_in_your'] = '在你的购物车里';
$lang['store_cart_no_items'] = '您的购物车中没有物品';

/*Soap Lang*/
$lang['soap_send_subject'] = '商店购买';
$lang['soap_send_body'] = '感谢您的选购!';

/*Email Lang*/
$lang['email_password_recovery'] = '密码恢复';
$lang['email_account_activation'] = '账户激活';

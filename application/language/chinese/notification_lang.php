<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$lang['notification_title_success'] = '成功';
$lang['notification_title_warning'] = '警告';
$lang['notification_title_error'] = '错误';
$lang['notification_title_info'] = '消息';

/*Notification Message (Login/Register) Lang*/
$lang['notification_username_empty'] = '用户名为空';
$lang['notification_email_empty'] = '邮箱为空';
$lang['notification_password_empty'] = '密码为空';
$lang['notification_user_error'] = '用户名或密码不正确。请再试一次！';
$lang['notification_email_error'] = '邮箱或密码不正确。请再试一次！';
$lang['notification_check_email'] = '用户名或邮箱不正确。请再试一次！';
$lang['notification_checking'] = '检查...';
$lang['notification_redirection'] = '正在连接到您的帐户...';
$lang['notification_new_account'] = '已创建新帐户。正在登录...';
$lang['notification_email_sent'] = '邮件已发送。请检查您的邮箱...';
$lang['notification_account_activation'] = '邮件已发送。请检查您的邮箱以激活您的帐户。';
$lang['notification_captcha_error'] = '请检查验证码';
$lang['notification_password_lenght_error'] = '密码长度错误。请使用5到16个字符之间的密码';
$lang['notification_account_already_exist'] = '此帐户已存在';
$lang['notification_password_not_match'] = '密码不匹配';
$lang['notification_same_password'] = '密码相同';
$lang['notification_currentpass_not_match'] = '旧密码不匹配';
$lang['notification_usernamepass_not_match'] = '密码与此用户名不匹配';
$lang['notification_used_email'] = '使用中的邮箱';
$lang['notification_email_not_match'] = '邮箱不匹配';
$lang['notification_username_not_match'] = '用户名不匹配';
$lang['notification_expansion_not_found'] = '未找到扩展';
$lang['notification_valid_key'] = '帐户已激活';
$lang['notification_valid_key_desc'] = '现在您可以使用您的帐户登录';
$lang['notification_invalid_key'] = '提供的激活密钥无效';

/*Notification Message (General) Lang*/
$lang['notification_email_changed'] = '邮箱已更改';
$lang['notification_username_changed'] = '用户名已更改';
$lang['notification_password_changed'] = '密码已更改';
$lang['notification_avatar_changed'] = '头像已更改';
$lang['notification_wrong_values'] = '参数错误';
$lang['notification_select_type'] = '选择类型';
$lang['notification_select_priority'] = '选择优先级';
$lang['notification_select_category'] = '选择分类';
$lang['notification_select_realm'] = '选择服务器';
$lang['notification_select_character'] = '选择角色';
$lang['notification_select_item'] = '选择物品';
$lang['notification_report_created'] = '报告已创建.';
$lang['notification_title_empty'] = '标题为空';
$lang['notification_description_empty'] = '描述为空';
$lang['notification_name_empty'] = '名称为空';
$lang['notification_id_empty'] = 'ID为空';
$lang['notification_reply_empty'] = '回复为空';
$lang['notification_reply_created'] = '回复已发送';
$lang['notification_reply_deleted'] = '回复已删除';
$lang['notification_topic_created'] = '主题已创建';
$lang['notification_donation_successful'] = '捐款已成功完成，请检查您帐户中的捐款点。';
$lang['notification_donation_canceled'] = '捐款已经取消';
$lang['notification_donation_error'] = '交易中提供的信息不匹配';
$lang['notification_store_chars_error'] = '选择项目中的角色';
$lang['notification_store_item_insufficient_points'] = '你没有足够的点数';
$lang['notification_store_item_purchased'] = '物品已购买，请查看游戏中的邮件。';
$lang['notification_store_item_added'] = '所选项目已添加到您的购物车中。';
$lang['notification_store_item_removed'] = '所选项目已从您的购物车中删除。';
$lang['notification_store_cart_error'] = '购物车更新失败，请重试。';

/*Notification Message (Admin) Lang*/
$lang['notification_changelog_created'] = 'The changelog has been created.';
$lang['notification_changelog_edited'] = 'The changelog has been edited.';
$lang['notification_changelog_deleted'] = 'The changelog has been deleted.';
$lang['notification_forum_created'] = 'The forum has been created.';
$lang['notification_forum_edited'] = 'The forum has been edited.';
$lang['notification_forum_deleted'] = 'The forum has been deleted.';
$lang['notification_category_created'] = 'The category has been created.';
$lang['notification_category_edited'] = 'The category has been edited.';
$lang['notification_category_deleted'] = 'The category has been deleted.';
$lang['notification_menu_created'] = 'The menu has been created.';
$lang['notification_menu_edited'] = 'The menu has been edited.';
$lang['notification_menu_deleted'] = 'The menu has been deleted.';
$lang['notification_news_deleted'] = 'The news has been deleted.';
$lang['notification_page_created'] = 'The page has been created.';
$lang['notification_page_edited'] = 'The page has been edited.';
$lang['notification_page_deleted'] = 'The page has been deleted.';
$lang['notification_realm_created'] = 'The realm has been created.';
$lang['notification_realm_edited'] = 'The realm has been edited.';
$lang['notification_realm_deleted'] = 'The realm has been deleted.';
$lang['notification_slide_created'] = 'The slide has been created.';
$lang['notification_slide_edited'] = 'The slide has been edited.';
$lang['notification_slide_deleted'] = 'The slide has been deleted.';
$lang['notification_item_created'] = 'The item has been created.';
$lang['notification_item_edited'] = 'The item has been edited.';
$lang['notification_item_deleted'] = 'The item has been deleted.';
$lang['notification_top_created'] = 'The top item has been created.';
$lang['notification_top_edited'] = 'The top item has been edited.';
$lang['notification_top_deleted'] = 'The top item has been deleted.';
$lang['notification_topsite_created'] = 'The topsite has been created.';
$lang['notification_topsite_edited'] = 'The topsite has been edited.';
$lang['notification_topsite_deleted'] = 'The topsite has been deleted.';

$lang['notification_settings_updated'] = 'The settings has been updated.';
$lang['notification_module_enabled'] = 'The module has been enabled.';
$lang['notification_module_disabled'] = 'The module has been disabled.';
$lang['notification_migration'] = 'The settings have been set.';

$lang['notification_donation_added'] = 'Added donation';
$lang['notification_donation_deleted'] = 'Deleted donation';
$lang['notification_donation_updated'] = 'Updated donation';
$lang['notification_points_empty'] = 'Points is empty';
$lang['notification_tax_empty'] = 'Tax is empty';
$lang['notification_price_empty'] = 'Price is empty';
$lang['notification_incorrect_update'] = 'Unexpected update';

$lang['notification_route_inuse'] = 'The route is already in use please choose another one.';

$lang['notification_account_updated'] = 'The account has been updated.';
$lang['notification_dp_vp_empty'] = 'DP/VP is empty';
$lang['notification_account_banned'] = 'The account has been banned.';
$lang['notification_reason_empty'] = 'Reason is empty';
$lang['notification_account_ban_remove'] = 'The ban in the account has been removed.';
$lang['notification_rank_empty'] = 'Rank is empty';
$lang['notification_rank_granted'] = 'The rank has been granted.';
$lang['notification_rank_removed'] = 'The rank has been deleted.';

$lang['notification_cms_updated'] = 'The CMS has been updated';
$lang['notification_cms_update_error'] = 'The CMS could not be updated';
$lang['notification_cms_not_updated'] = 'A new version has not been found to update';

$lang['notification_select_category'] = '选择类别';
